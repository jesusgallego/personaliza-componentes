package es.ua.eps.personalizacomponentes;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by mastermoviles on 15/10/16.
 */
public class Grafica extends View{

    private int percentage;

    private RectF rect = new RectF();
    private Paint paint = new Paint();

    public Grafica(Context context) {
        super(context);
    }

    public Grafica(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public Grafica(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.Grafica);

        this.percentage = ta.getInt(R.styleable.Grafica_percentage, 0);
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
        invalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (w > h)
        {
            rect.set(w / 2 - h / 2, 0, w / 2 + h / 2, h);
        }
        else
        {
            rect.set(0, h / 2 - w / 2, w, h / 2 + w / 2);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawColor(Color.WHITE);


        paint.setStyle(Paint.Style.FILL_AND_STROKE);

        paint.setColor(Color.RED);
        canvas.drawArc(rect, 0, 360 * percentage / 100, true, paint);

        paint.setColor(Color.BLUE);
        canvas.drawArc(rect, 360 * percentage / 100, 360 - 360 * percentage / 100, true, paint);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);
        int width = 50;
        int height = 50;

        switch(widthMode) {
            case MeasureSpec.EXACTLY:
                width = widthSize;
                break;
            case MeasureSpec.AT_MOST:
                if(width > widthSize) { width = widthSize;
                }
                break;
        }
        switch(heightMode) {
            case MeasureSpec.EXACTLY:
                height = heightSize;
                break;
            case MeasureSpec.AT_MOST:
                if(height > heightSize) {
                    height = heightSize;
                }
                break;
        }
        this.setMeasuredDimension(width, height);

    }
}
