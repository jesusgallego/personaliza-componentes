package es.ua.eps.personalizacomponentes;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

/**
 * Created by mastermoviles on 15/10/16.
 */
public class TextViewCitas extends TextView {

    static final private String[] citas = {
            "La felicidad humana generalmente no se logra con grandes golpes de suerte, que pueden ocurrir pocas veces, sino con pequeñas cosas que ocurren todos los días.",
            "Felicidad no es hacer lo que uno quiere sino querer lo que uno hace.",
            "Un amigo es uno que lo sabe todo de ti y a pesar de ello te quiere.",
            "La amistad es un alma que habita en dos cuerpos; un corazón que habita en dos almas."
    };


    public TextViewCitas(Context context) {
        super(context);
        init();
    }

    public TextViewCitas(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewCitas(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        nuevaCita();

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                nuevaCita();
            }
        });
    }

    private void nuevaCita() {
        int i = new Random().nextInt(citas.length);
        this.setText(citas[i]);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

}
