package es.ua.eps.personalizacomponentes;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

/**
 * Created by mastermoviles on 15/10/16.
 */
public class EdicionBorrable extends LinearLayout {

    EditText editText;
    Button button;

    public EdicionBorrable(Context context) {
        super(context);
        init();
    }

    public EdicionBorrable(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EdicionBorrable(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.edicion_borrable, this, true);

        editText = (EditText)findViewById(R.id.editText);
        button = (Button)findViewById(R.id.editButton);

        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("");
            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }
}
